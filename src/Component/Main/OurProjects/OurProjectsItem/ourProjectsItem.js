import React from "react";
import './ourProjectsItem.css'
import ImgProject from './project-img.svg'

export default function OurProjectsItem() {
    return (
        <li className="projects-item">
            <div className="wrap">
                <img src={ImgProject} alt="project-images"/>
            </div>
            <span className="project-title"> Арена</span>
            <p className="projects-paragraph">Мы сделали самую красивую арену в Европе. Это открытие стало для нас
                прорывной точкой для разивтия на следующие десятилетия. Мы очень рады данному еву.</p>
        </li>
    )
}