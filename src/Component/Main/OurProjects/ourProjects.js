import React from "react";
import './ourProjects.css'
import OurProjectsItem from "./OurProjectsItem/ourProjectsItem";

export default function OurProjects() {
    return (
        <>
            <h2 className="title">НАШИ САМЫЕ БОЛЬШИЕ ПРОЕКТЫ</h2>
            <ul className="projects-list">
                <OurProjectsItem />
                <OurProjectsItem />
                <OurProjectsItem />
            </ul>
        </>
    )
}