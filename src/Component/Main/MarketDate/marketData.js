import React from "react";
import './marketDate.css'

export default function MarketData() {
    return (
      <li className="market-dates-item">
          <span className="item-number">26</span>
          <span className="item-years">лет</span>
          <span className="item-market">на рынке</span>
      </li>
    );
}