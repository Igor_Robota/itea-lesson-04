import React from "react";
import './main.css'
import MarketData from "./MarketDate/marketData";
import OurProjects from "./OurProjects/ourProjects";

export default function Main() {
    return (
        <div className="container">
            <ul className="market-dates-list">
                <MarketData />
                <MarketData />
                <MarketData />
                <MarketData />
            </ul>
            <div className="our-project">
                <OurProjects />
            </div>
        </div>
    )
}