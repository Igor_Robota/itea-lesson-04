import React from "react"
import './app.css'

const App = () => {
    return (
        <div className="container">
            <div className="head-wrap">
                <div className="head-wrap-left">
                    <a href="#" className="icon" />
                </div>
                <button className="hamburger-menu-box">
                    <span className="hamburger" />
                </button>
            </div>
            <div className="head-title">
                <h1 className="head-header">РЕАЛИЗУЕМ КРУПНЕЙШИЕ ПРОЕКТЫ В Украине</h1>
                <p className="head-paragraph">стадионы, газопроводы, мосты, дамбы</p>
            </div>
        </div>
    );
}

export default App