import React from "react";
import './footer.css'

export default function Footer() {
    return (
        <div className="container">
            <div className="footer-wrap">
                <div>
                    <span className="footer-title">САМЫЕ УМНЫЕ ПРОЕКТЫ</span><br/>
                    <span className="footer-paragraph">РЕАЛИЗУЕМ САМЫЕ СМЕЛЫЕ РЕШЕНИЯ</span>
                </div>
                <button className="footer-btn">ВАШ ЗАПРОС</button>
            </div>
        </div>
    )
}