import React from 'react'
import ReactDOM from 'react-dom'
import App from "./Component/App/app"
import './index.css'
import Main from "./Component/Main/main";
import Footer from "./Component/Footer/footer";

const Hello = () => {
    return (
        <>
            <header className="header">
                <App />
            </header>
            <main>
                <Main />
            </main>
            <footer className="footer">
                <Footer />
            </footer>
        </>
    );
}

ReactDOM.render(<Hello />,document.getElementById('root'));

